<?php

namespace User\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use Core\Service\ServiceInterface;
use User\Entity\User;
use User\Service\UserService;

class ActivationPageAction
  implements ServerMiddlewareInterface
{
  /**
   * @var EntityManager
   */
  protected $entityManager;
  /**
   * @var UserService
   */
  protected $service;
  /**
   * @var User
   */
  protected $entity = User::class;

  public function __construct(EntityManager $entityManager, ServiceInterface $service)
  {
    $this->entityManager = $entityManager;
    $this->service = $service;
  }

  public function process(ServerRequestInterface $request, DelegateInterface $delegate)
  {

    $id = $request->getAttribute('id');
    $key = $request->getAttribute('key');
    try{
      $result = $this->service->activate($id, $key);
      if ($result) {
        return new JsonResponse([
          'success' => true
        ]);
      }
    }catch(\Exception $e){}

    return new JsonResponse([
      'success' => false,
      'message' => 'A chave ' . $key . ' é inválida',
    ]);
  }
}
