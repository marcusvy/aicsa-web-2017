<?php

namespace User;

use Core\Doctrine\Helper\ConfigProviderHelper;

/**
 * The configuration provider for the User module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
  /**
   * Returns the configuration array
   *
   * To add a bit of a structure, each section is defined in a separate
   * method which returns an array with its configuration.
   *
   * @return array
   */
  public function __invoke()
  {
    return [
      'dependencies' => $this->getDependencies(),
      'templates' => $this->getTemplates(),
      'doctrine' => $this->getDoctrine(),
      'form_elements' => $this->getFormElements(),
    ];
  }

  /**
   * Returns the container dependencies
   *
   * @return array
   */
  public function getDependencies()
  {
    return [
      'invokables' => [
      ],
      'factories' => [
        Action\ActivationPageAction::class => Action\ActivationPageFactory::class,
        Action\UserPageAction::class => Action\UserPageFactory::class,
        Action\UserPerfilPageAction::class => Action\UserPerfilPageFactory::class,
        Action\UserRolePageAction::class => Action\UserRolePageFactory::class,
        Adapter\AuthAdapter::class => Adapter\AuthAdapterFactory::class,
        Service\AuthService::class => Service\AuthServiceFactory::class,
        Service\UserService::class => Service\UserServiceFactory::class,
        Service\UserPerfilService::class => Service\UserPerfilServiceFactory::class,
        Service\UserRoleService::class => Service\UserRoleServiceFactory::class,
      ],
    ];
  }

  /**
   * Returns the Form configuration
   *
   * @return array
   */
  public function getFormElements()
  {
    return [
      'invokables' => [
      ],
      'factories' => [
          Form\Fieldset\Role::class => Form\Fieldset\RoleFactory::class,
      ],
    ];
  }

  /**
   * Returns the templates configuration
   *
   * @return array
   */
  public function getTemplates()
  {
    return [
      'paths' => [
        'app' => [__DIR__ . '/../templates/app'],
        'error' => [__DIR__ . '/../templates/error'],
        'layout' => [__DIR__ . '/../templates/layout'],
      ],
    ];
  }

  /**
   * Returns the doctrine configuration
   *
   * @return array
   */
  public function getDoctrine()
  {
    $helper = new ConfigProviderHelper();
    return $helper->generate(
      __NAMESPACE__,
      __DIR__ . '/Entity'
    );
  }
}
