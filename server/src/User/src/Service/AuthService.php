<?php
namespace User\Service;

use User\Adapter\AuthAdapter;
use Zend\Authentication\Adapter;
use Zend\Authentication\Result;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\AuthenticationServiceInterface;
use Zend\Authentication\Storage;

class AuthService
  extends AuthenticationService
  implements AuthenticationServiceInterface
{

  /**
   * AuthService constructor.
   * @param Storage\StorageInterface $storage
   * @param Adapter\AdapterInterface $adapter
   */
  public function __construct(
    Storage\StorageInterface $storage,
    Adapter\AdapterInterface $adapter)
  {
    parent::__construct($storage, $adapter);
  }

  /**
   * @param $identity
   * @param $credential
   * @return AuthAdapter
   */
  public function setup($identity, $credential)
  {
    return $this->getAdapter()
      ->setIdentity($identity)
      ->setCredential($credential);
  }

  /**
   * @return Result
   */
  public function authenticate()
  {
    return parent::authenticate($this->getAdapter());
  }


}
