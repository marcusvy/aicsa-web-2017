<?php

namespace App\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;


class HomeSlidePageAction implements ServerMiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
      $slides = [
        [
          'title' => 'Educação em 1º Lugar',
          'description' => 'O Brasil se movimenta para uma educação melhor, contamos com a sua presença',
          'image' => '/local/slides/slide_sample.jpg'
        ]
      ];
      return new JsonResponse([
        'collection' => $slides
      ]);
    }
}
