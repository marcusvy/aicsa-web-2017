<?php
/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Action\HomePageAction::class, 'home');
 * $app->post('/album', App\Action\AlbumCreateAction::class, 'album.create');
 * $app->put('/album/:id', App\Action\AlbumUpdateAction::class, 'album.put');
 * $app->patch('/album/:id', App\Action\AlbumUpdateAction::class, 'album.patch');
 * $app->delete('/album/:id', App\Action\AlbumDeleteAction::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Action\ContactAction::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */

$app->get('/', App\Action\HomePageAction::class, 'home');
$app->get('/api/ping', App\Action\PingAction::class, 'api.ping');


/**
 * User Module
 */
$app->route('/api/user[/{id:\d+}]', User\Action\UserPageAction::class, ['GET', 'POST', 'PUT', 'DELETE'], 'user');
$app->route('/api/user-perfil[/{id:\d+}]', User\Action\UserPerfilPageAction::class, ['GET', 'POST', 'PUT', 'DELETE'], 'user.perfil');
$app->route('/api/user-role[/{id:\d+}]', User\Action\UserRolePageAction::class, ['GET', 'POST', 'PUT', 'DELETE'], 'user.role');
$app->get('/api/user/activate/{id:\d+}/{key}', User\Action\ActivationPageAction::class, 'user.activation');


/**
 * Adunir
 */
$app->get('/api/adunir/slides', App\Action\HomeSlidePageAction::class,'adunir.slides');
