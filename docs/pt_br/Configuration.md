Configuração
==========================

##Oracle Database
**Oracle Database 11g Release 2 Express Edition for Linux x64: [oracle-xe-11.2.0-1.0.x86_64.rpm.zip](http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html).**

#Construção de imagens

Antes construir a imagem, tenha certeza de providenciar a instalação de binários e por eles dentro do diretório correto.
Então execute o seguinte script como root ou com privilégios `sudo` no diretório do projeto:

```bash
sh buildDockerImage.sh -v 11.2.0.2 -x -i
```

##Atualizar configuração do docker
In **docker-compose.yml** update the service *db* to provide the database service.

```yaml
db:
    image: oracle/database:11.2.0.2
    volumes:
      - ./database/oracle/oradata:/opt/oracle/oradata # persistent oracle database data.
    ports:
      - 1521:1521
      - 8080:8080
      - 5500:5500
```
