#Ambiente de Desenvolvimento

## Sistema
* SO Linux: Debian 8 and Oracle Linux 7
* Docker: Virtualização de Ambiente
* Node (7)
* PHP (7)
* MySQL (5.7)

## Frameworks
* Angular 4
* Zend Framework 3
* Zend Expressive

## Preprocessadores
* Typescript 2
* Sass
* PostCSS

## CLI
* Angular CLI

## Gerenciador de Pacotes
* Npm
* Composer
