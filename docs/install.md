Instalation
==========================

## Server

Run bash on server:
```bash
docker-compose exec server /bin/bash
```
On docker container *server*:
```bash
cd app
composer install
```

## Server
