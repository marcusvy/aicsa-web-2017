MVinicius Orion Boilerplate
==========================

## Project

Clone git project:
```bash
git clone https://github.com/marcusvy/mv-orion-boilerplate.git
cd mv-orion-boilerplate
```

Start docker services
```bash
docker-compose up -d
```

## Ports
Server
* 8000: Apache Server
* 9000: PHP-FPM socket address
Database
* 3306: Mysql Server
* 5500: Oracle Enterprise Manager Database Control
* 1521: Connect to the Oracle Database.
* 8080: Oracle Application Express (APEX).
Client
* 4200: Angular Cli Server
* 49152: Webpack live reload


## Author

**Adm. Marcus Vinícius R G Cardoso**
(CEO & Founder Mvinicius Consultoria)

- E-mail: <mailto:marcus@mviniciusconsultoria.com.br>
- Site: <http://mviniciusconsultoria.com.br>

## Copyright

2013-2017 MVinicius Consultoria, by
[MIT License](http://opensource.org/licenses/MIT).
Documentation under [MIT Licence](http://opensource.org/licenses/MIT).
