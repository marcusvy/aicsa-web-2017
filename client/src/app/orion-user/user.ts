export class User {
  id: number;
  name: string;

  constructor(obj: Object = {}) {
    Object.assign(this, {}, obj);
  }
}
