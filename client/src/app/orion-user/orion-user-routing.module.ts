import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { OrionUserComponent } from './orion-user.component';
import { OrionUserListComponent } from "./orion-user-list/orion-user-list.component";

const routes: Routes = [
  {
    path: 'user',
    component: OrionUserComponent,
    children: [
      {path: '', component: OrionUserListComponent},
      {path: 'list', component: OrionUserListComponent},
      // {path: 'form', component: SgdEventoFormComponent},
      // {path: 'form/:id', component: SgdEventoFormComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ]
})
export class OrionUserRoutingModule {
}
