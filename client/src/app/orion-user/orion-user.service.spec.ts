/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { OrionUserService } from './orion-user.service';

describe('OrionUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrionUserService]
    });
  });

  it('should ...', inject([OrionUserService], (service: OrionUserService) => {
    expect(service).toBeTruthy();
  }));
});
