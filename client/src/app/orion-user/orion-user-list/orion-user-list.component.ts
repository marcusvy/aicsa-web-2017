import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { Pagination } from '../../ui/pagination/pagination';
import { User } from "../user";
import { OrionUserService } from '../orion-user.service';
import { DataListComponent } from "../../ui/data/data-list/data-list.component";
import { BehaviorSubject, Subscription } from "rxjs/Rx";

import { PaginationService } from '../../ui/pagination/pagination.service';

@Component({
  selector: 'mv-orion-user-list',
  templateUrl: './orion-user-list.component.html',
  styleUrls: ['./orion-user-list.component.scss']
})
export class OrionUserListComponent implements OnInit, OnDestroy {

  private _entity: any = new BehaviorSubject({});
  private subscriptionFetchAll: Subscription;
  private subscriptionSearch: Subscription;
  public _collection: BehaviorSubject<User[]> = new BehaviorSubject([]);
  modeEdit: boolean;
  modeForm: boolean;
  modeDelete: boolean;
  modeSearch: boolean;
  formSearch: FormGroup;
  @ViewChild(DataListComponent) dataList: DataListComponent;

  constructor(private router: Router,
              private service: OrionUserService) {
  }

  get entity() {
    return this._entity.asObservable();
  }

  get collection() {
    return this._collection.asObservable();
  }

  ngOnInit() {
    this.onLoadCollection();
  }

  ngOnDestroy() {
    if (this.subscriptionFetchAll) {
      this.subscriptionFetchAll.unsubscribe();
    }
    if (this.subscriptionSearch) {
      this.subscriptionSearch.unsubscribe();
    }
  }

  onModeEdit(value) {
    this.modeEdit = value;
  }

  onModeForm(value) {
    this.modeForm = value;
  }

  onModeDelete(value) {
    this.modeDelete = value;
  }

  onModeSearch(value) {
    this.modeSearch = value;
  }

  //Link form to DataList
  onFormSearchInit(formSearch) {
    this.formSearch = formSearch;
  }

  // onCreate(event) {
  // this.router.navigate(['form']);
  // }

  // onUpdate(event) {
  // this.router.navigate(['form']);
  // }

  onDelete(event) {
    console.log(event);
  }

  // onDeleteConfirm($event) {
  //   this._entity.next($event);
  // }

  onOpenModalDelete($event) {
    this.dataList.onOpenModalDelete($event);
  }

  /**
   * Crud Operations
   * =================================================
   */

  onSearch(parameters) {
    this.subscriptionSearch = this.service.api
      .search(parameters)
      .subscribe(data => {
        // this.onEnableModeSearch();
        this.modeSearch = true;
        this._collection.next(data.collection);
        // this.success = data.success;
        // this.error = false;
      }, (error) => {
        // this.error = true;
      });
  }

  /**
   * LoadCollection
   */
  onLoadCollection() {
    this.subscriptionFetchAll = this.service.api
      .fetchAll()
      .subscribe((data: any) => {
        this._collection.next(data.collection);
        // this.success = data.success;
        // this.error = false;
      }, (error) => {
        // this.error = true;
      });
  }

  onSave($event) {
    // this.dataList.onCloseModalForm();
    console.log("onSave", $event);
  }
}
