import { Injectable } from '@angular/core';

import { ApiHttpService } from '../core/http/api-http.service';

@Injectable()
export class OrionUserService {

  public api;
  private storagePrefix: string = 'orion::user';
  private url: string = '/api/user';

  constructor(private http: ApiHttpService) {
    this.api = http.factory(this.url, this.storagePrefix);
  }
}
