import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';

import { OrionUserRoutingModule } from './orion-user-routing.module';

import { OrionUserComponent } from './orion-user.component';
import { OrionUserListComponent } from './orion-user-list/orion-user-list.component';
import { OrionUserListItemComponent } from './orion-user-list-item/orion-user-list-item.component';
import { OrionUserFormComponent } from './orion-user-form/orion-user-form.component';

import { OrionUserService } from './orion-user.service';

@NgModule({
  imports: [
    CoreModule,
    OrionUserRoutingModule,
  ],
  declarations: [
    OrionUserComponent,
    OrionUserListComponent,
    OrionUserListItemComponent,
    OrionUserFormComponent
  ],
  exports: [
    OrionUserComponent
  ],
  providers: [
    OrionUserService
  ]
})
export class OrionUserModule {
}
