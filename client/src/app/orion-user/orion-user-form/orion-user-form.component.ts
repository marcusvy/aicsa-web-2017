import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ApiHttpService } from '../../core/http/api-http.service';
import { CommonResponse } from '../../core/http/response/common-response';
import { OrionUserService } from '../orion-user.service';
import { Subscription } from 'rxjs/Rx';
// import { Router, ActivatedRoute } from "@angular/router";
// import { MomentPipe } from '../../core/pipe/moment.pipe';

@Component({
  selector: 'mv-orion-user-form',
  templateUrl: './orion-user-form.component.html',
  styleUrls: ['./orion-user-form.component.scss']
})
export class OrionUserFormComponent implements OnInit {

  private id: number = 0;
  public editMode = false;
  public form: FormGroup;
  private submitted = false;
  private subscriptionFetch: Subscription;
  private subscriptionCreate: Subscription;
  private subscriptionUpdate: Subscription;
  public success = false;
  public error = false;
  public onSave: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private fb: FormBuilder,
    private service: OrionUserService) {
    this.formBuilder();
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.subscriptionCreate) {
      this.subscriptionCreate.unsubscribe();
    }
    if (this.subscriptionUpdate) {
      this.subscriptionUpdate.unsubscribe();
    }
    if (this.subscriptionFetch) {
      this.subscriptionFetch.unsubscribe();
    }
  }

  formBuilder() {
    this.form = this.fb.group({
      credential: ['marcus'+(Math.random()).toString().slice(2,6), Validators.compose([
        Validators.required
      ])],
      password: ['123456', Validators.compose([
        Validators.required
      ])],
      status: ['ativo', Validators.compose([
        Validators.required
      ])],
      active: [true, Validators.compose([
        Validators.required
      ])],
    });
  }

  formPopulate(entity) {
    return this.form.setValue({
      credential: entity.credential,
      password: entity.password,
      status: entity.status,
      active: entity.active,
    });
  }


  reset() {
    this.form.reset();
  }
  /**
   * Crud Operations
   */

  loadEntity() {
    this.subscriptionFetch = this.service.api
      .fetch(this.id)
      .subscribe((data: CommonResponse) => {
        if (data.success) {
          const entity = data.collection[0];
          //Populate form
          this.formPopulate(entity);
          this.success = data.success;
        }
      }, () => {
        this.error = true;
      });
  }
  onSubmit($event) {
    if (this.form.valid) {
      if (this.id) {
        const entity = this.form.value;
        entity.id = this.id;
        this.subscriptionUpdate = this.service.api
          .update(entity)
          .subscribe((data: CommonResponse) => {
            if (data.success) {
              this.onSave.emit(data);
            }
          }, (error) => {
            // console.log("erro:", error);
          });
      } else {
        this.subscriptionCreate = this.service.api
          .create(this.form.value)
          .subscribe((data: CommonResponse) => {
            if (data.success) {
              this.onSave.emit(data);
              console.log("criado", data);
            }
          }, (error) => {
            // console.log("erro:", error);
          });
      }
      this.submitted = true;
    }
  }
}

