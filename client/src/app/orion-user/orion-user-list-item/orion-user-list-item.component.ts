import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'mv-orion-user-list-item',
  templateUrl: './orion-user-list-item.component.html',
  styleUrls: ['./orion-user-list-item.component.scss']
})
export class OrionUserListItemComponent implements OnInit {

  @Input() entity;
  @Input() modeEdit: boolean = true;
  @Input() modeDelete: boolean = true;
  @Output() onOpenModal: EventEmitter<any> = new EventEmitter();

  constructor() {

  }

  ngOnInit() {

  }

  onConfirmDelete(){
    // console.log(this.entity);
    this.onOpenModal.emit(this.entity);
  }

}
