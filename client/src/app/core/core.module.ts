import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { UiModule } from '../ui/ui.module';
import { ConnectionService } from './connection.service';
import { ApiHttpService } from './http/api-http.service';
import { MomentPipe } from './pipe/moment.pipe';
import { CpfPipe } from './pipe/cpf.pipe';
import { PhonePipe } from './pipe/phone.pipe';
import { BoletoPipe } from './pipe/boleto.pipe';
import { CepPipe } from './pipe/cep.pipe';
import { CnpjPipe } from './pipe/cnpj.pipe';

@NgModule({
  declarations: [
    MomentPipe,
    CpfPipe,
    PhonePipe,
    BoletoPipe,
    CepPipe,
    CnpjPipe,

  ],
  exports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    UiModule,
    MomentPipe,
    CpfPipe,
    PhonePipe,
    BoletoPipe,
    CepPipe,
    CnpjPipe,
  ],
  providers: [
    ConnectionService,
    ApiHttpService,
    MomentPipe,
  ],
})
export class CoreModule { }
