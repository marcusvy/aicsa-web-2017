import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'moment'
})
export class MomentPipe implements PipeTransform {

  transform(value: any, format?: string): any {

    let jsDateString = '';
    if(value instanceof Object && value.hasOwnProperty('date')){
      if(/^\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}\.\d{6}$/.test(value.date)){
        jsDateString = value.date.replace(/(\.\d{6})$/,'');
      }else{
        jsDateString = value.date;
      }
    }else{
      jsDateString = value;
    }
    format = (format!== undefined) ? format : 'L LTS';
    return moment(jsDateString).format(format);
  }

}
