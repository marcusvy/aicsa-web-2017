import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod, URLSearchParams, QueryEncoder } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { ConnectionService } from "../connection.service";

@Injectable()
export class ApiHttpService {

  private http: Http;
  private online: boolean = false;
  private _storage: BehaviorSubject<any[]> = new BehaviorSubject<any>([]);
  private storagePrefix: string = 'http::'; //deve ser ajustada
  private url: string = ''; //deve ser ajustada

  constructor(http: Http, connection: ConnectionService) {
    this.http = http;
    connection.online.subscribe(online => this.online = online);
  }

  factory(url, storagePrefix) {
    this.url = url;
    this.storagePrefix = storagePrefix;
    return this;
  }

  get storage() {
    return this._storage.asObservable();
  }

  storageSetItem(data) {
    let key = `${this.storagePrefix}`;
    localStorage.setItem(key, JSON.stringify(data));
  }

  storageGetItem() {
    let key = `${this.storagePrefix}`;
    return JSON.parse(localStorage.getItem(key));
  }

  fetch(id: number) {
    return (this.online) ? this.fetchOnline(id) : this.fetchStorage(id);
  }

  fetchOnline(id: number) {
    let url = `${this.url}/${id}`;
    return this.http.get(url)
      .map((res: Response) => res.json())
      .catch(this.errorHandler);
  }

  fetchStorage(id: any) {
    return this.storage
      .flatMap((response) => response)
      .filter(evento => evento.id == id)
      .catch(this.errorHandler);
  }

  fetchAll() {
    return (this.online) ? this.fetchAllOnline() : this.fetchAllStorage();
  }

  fetchAllOnline() {
    return this.http.get(this.url)
      .map((res: Response) => res.json())
      .catch(this.errorHandler);
  }

  fetchAllStorage() {
    let list = this.storageGetItem();
    this._storage.next(list);
    return this.storage;
  }

  create(entity: any) {
    let body = JSON.stringify(entity);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.url, body, options)
      .map((res: Response) => res.json())
      .catch(this.errorHandler);
  }

  update(entity: any) {
    let body = JSON.stringify(entity);
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    let url = `${this.url}/${entity.id}`;
    return this.http.put(url, body, options)
      .map((res: Response) => res.json())
      .catch(this.errorHandler);
  }

  delete(id: number) {
    let url = `${this.url}/${id}`;
    return this.http.delete(url)
      .map((res: Response) => res.json())
      .catch(this.errorHandler);
  }

  search(params: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    /** @todo ecrypt */
    let search = new URLSearchParams();
    for (let p in params) {
      search.append(p, params[p]);
    }

    let options = new RequestOptions({
      headers: headers,
      search: search,
    });
    return this.http.get(this.url, options)
      .map((res: Response) => res.json())
      .catch(this.errorHandler);
  }

  errorHandler(error: any) {
    return Observable.throw(error.json().error || 'Server error');
  }

}
