export interface CommonResponse {
  success: boolean;
  collection: any[];
  message: string;
  messages: string[];
}
