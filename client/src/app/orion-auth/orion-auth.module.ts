import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { OrionAuthRoutingModule } from "./orion-auth-routing.module";

import { OrionAuthLoginComponent } from './orion-auth-login/orion-auth-login.component';
import { OrionAuthRegisterComponent } from './orion-auth-register/orion-auth-register.component';
import { OrionAuthLostComponent } from './orion-auth-lost/orion-auth-lost.component';
import { OrionAuthComponent } from "./orion-auth.component";

@NgModule({
  imports: [
    CoreModule,
    OrionAuthRoutingModule
  ],
  declarations: [
    OrionAuthComponent,
    OrionAuthLoginComponent,
    OrionAuthRegisterComponent,
    OrionAuthRegisterComponent,
    OrionAuthLostComponent,
  ],
  exports: [
    OrionAuthComponent
  ]
})
export class OrionAuthModule {
}
