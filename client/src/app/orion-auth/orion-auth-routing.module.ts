import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import {OrionAuthComponent} from './orion-auth.component';
import {OrionAuthLoginComponent} from "./orion-auth-login/orion-auth-login.component";
import {OrionAuthRegisterComponent} from "./orion-auth-register/orion-auth-register.component";
import {OrionAuthLostComponent} from "./orion-auth-lost/orion-auth-lost.component";

const routes: Routes = [
  {
    path: 'auth',
    component: OrionAuthComponent,
    children: [
      {path: '', component: OrionAuthLoginComponent},
      {path: 'register', component: OrionAuthRegisterComponent},
      {path: 'lostpassword', component: OrionAuthLostComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ]
})
export class OrionAuthRoutingModule {
}
