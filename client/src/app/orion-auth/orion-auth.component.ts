import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-orion-auth',
  templateUrl: './orion-auth.component.html',
  styleUrls: ['./orion-auth.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class OrionAuthComponent implements OnInit, OnDestroy {

  constructor() { }

  ngOnInit() {

    document.body.classList.add('orion-login');
  }

  ngOnDestroy() {
    document.body.classList.remove('orion-login');
  }

}
