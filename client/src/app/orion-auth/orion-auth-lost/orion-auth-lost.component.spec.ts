/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { OrionAuthLostComponent } from './orion-auth-lost.component';

describe('OrionAuthLostComponent', () => {
  let component: OrionAuthLostComponent;
  let fixture: ComponentFixture<OrionAuthLostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrionAuthLostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrionAuthLostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
