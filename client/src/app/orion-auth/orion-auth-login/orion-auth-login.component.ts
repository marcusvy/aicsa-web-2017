import { Component, OnInit, OnDestroy, ViewChild, Renderer } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from "@angular/router";

@Component({
  selector: 'mv-orion-auth-login',
  templateUrl: './orion-auth-login.component.html',
  styleUrls: ['./orion-auth-login.component.scss']
})
export class OrionAuthLoginComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  public submitted: boolean = false;
  public invalidAuthentication: boolean = false;
  @ViewChild('userInput') userInput;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private renderer: Renderer
  ) { }

  ngOnInit() {
    this.formBuilder();
  }

  ngOnDestroy() {

  }

  formBuilder() {
    this.form = this.fb.group({
      'user': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])],
    });
  }

  onSubmit($event) {
    if (this.form.valid) {
      let modal = this.form.value;

      // if (modal.password === '@tne#2017' && modal.user === 'alexandre') {
        this.router.navigate(['/orion']);
      // }
    }
    this.renderer.invokeElementMethod(this.userInput.nativeElement, 'focus');
    this.form.reset();
    this.invalidAuthentication = true;
    this.submitted = true;
  }
}
