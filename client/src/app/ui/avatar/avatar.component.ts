import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'mv-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnInit {

  @Input() src: string = '';

  constructor() { }

  ngOnInit() {
    if(this.src===''){
      this.src = '/assets/img/user_m_shield.png';
    }
  }

}
