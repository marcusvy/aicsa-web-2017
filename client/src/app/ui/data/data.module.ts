import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule } from "../layout/layout.module";
import { ButtonModule } from "../button/button.module";
import { IconModule } from "../icon/icon.module";
import { ListModule } from "../list/list.module";
import { MenuModule } from "../menu/menu.module";
import { ToolbarModule } from "../toolbar/toolbar.module";
import { ModalModule } from "../modal/modal.module";

import { DataComponent } from './data.component';
import { DataListComponent } from './data-list/data-list.component';
import { DataListModalSearchComponent } from './data-list/data-list-modal-search/data-list-modal-search.component';
import { DataListModalDeleteComponent } from './data-list/data-list-modal-delete/data-list-modal-delete.component';
import { DataListFormSearchComponent } from './data-list/data-list-form-search/data-list-form-search.component';

import { DataListActionDirective } from './data-list/data-list-action.directive';
import { DataListSearchDirective } from './data-list/data-list-search.directive';
import { DataListDeleteDirective } from './data-list/data-list-delete.directive';
import { DataListFormDirective } from './data-list/data-list-form.directive';
import { DataListCollectionDirective } from './data-list/data-list-collection.directive';
import { DataListModalFormComponent } from './data-list/data-list-modal-form/data-list-modal-form.component';
import { DataPaginationDirective } from './data-pagination.directive';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    ButtonModule,
    IconModule,
    ListModule,
    MenuModule,
    ModalModule,
    ToolbarModule,
  ],
  declarations: [
    DataComponent,
    DataListComponent,
    DataListModalSearchComponent,
    DataListModalDeleteComponent,
    DataListFormSearchComponent,
    DataListActionDirective,
    DataListSearchDirective,
    DataListDeleteDirective,
    DataListFormDirective,
    DataListCollectionDirective,
    DataListModalFormComponent,
    DataPaginationDirective,
  ],
  exports: [
    DataComponent,
    DataListComponent,
    DataListFormSearchComponent,
    DataListActionDirective,
    DataListSearchDirective,
    DataListDeleteDirective,
    DataListCollectionDirective,
    DataListFormDirective,
    DataListModalFormComponent,
    DataPaginationDirective,
  ]
})
export class DataModule { }
