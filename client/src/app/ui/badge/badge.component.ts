import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BadgeComponent implements OnInit {

  @Input() value: string = '';

  constructor() { }

  ngOnInit() {
  }

}
