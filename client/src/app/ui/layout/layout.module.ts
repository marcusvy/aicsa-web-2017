import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { GridDirective } from './grid.directive';
import { ContainerDirective } from './container.directive';
import { RowDirective } from './row.directive';
import { ColDirective } from './col.directive';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LayoutComponent,
    GridDirective,
    ContainerDirective,
    RowDirective,
    ColDirective,
  ],
  exports: [
    LayoutComponent,
    GridDirective,
    ContainerDirective,
    RowDirective,
    ColDirective,
  ]
})
export class LayoutModule { }
