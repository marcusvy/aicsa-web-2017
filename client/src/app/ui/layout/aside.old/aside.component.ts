import {
  Component,
  Input,
  HostBinding,
  OnInit,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import {
  animate,
  trigger,
  state,
  transition,
  style
} from '@angular/animations';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'mv-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('asideAnimation', [
      state('opened', style({ 'z-index': 1000, opacity: 1 })),
      state('closed', style({ diplay: 'none', opacity: 0 })),
      transition('opened <=> closed', animate(300)),
    ]),
    trigger('contentAnimation', [ //content
      state('opened', style({ diplay: 'flex', transform: 'translateX(0)', opacity: 1 })),
      state('closed', style({ diplay: 'none', transform: 'translateX(-100%)', opacity: 0 })),
      transition('opened <=> closed', animate(300)),
    ]),
    trigger('backdropAnimation', [ //backdrop
      state('opened', style({ diplay: 'block', opacity: 1 })),
      state('closed', style({ diplay: 'none', opacity: 0 })),
      transition('opened <=> closed', animate(500)),
    ]),
  ],
})
export class AsideComponent implements OnInit, OnDestroy {

  @Input() id?: string = '';
  @Input() fixed? = false;
   _opened: boolean;

  constructor(private router:Router) { }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd && this._opened && !this.fixed) {
        this.close();
      }
    });
  }

  ngOnDestroy() {

  }

  /**
   * Aside Actions
   */
  close() {
    this._opened = false;
  }
  open() {
    this._opened = true;
  }
  toggle() {
    this._opened = !this._opened;
  }

  /**
   * Classes
   */
  @HostBinding('@asideAnimation')
  get opened() {
    return (this._opened) ? 'opened' : 'closed';
  }

}
