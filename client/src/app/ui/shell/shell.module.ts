import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsideModule } from '../aside/aside.module';
import { ButtonModule } from '../button/button.module';
import { DeviceModule } from '../device/device.module';
import { IconModule } from '../icon/icon.module';
import { LayoutModule } from '../layout/layout.module';
import { MenuModule } from '../menu/menu.module';
import { ToolbarModule } from '../toolbar/toolbar.module';

import { ShellComponent } from './shell.component';
import { ShellMenuDirective } from './shell-menu.directive';
import { ShellContentDirective } from './shell-content.directive';
import { ShellToolDirective } from './shell-tool.directive';

@NgModule({
  imports: [
    CommonModule,
    AsideModule,
    ButtonModule,
    DeviceModule,
    IconModule,
    LayoutModule,
    MenuModule,
    ToolbarModule,
  ],
  declarations: [
    ShellComponent,
    ShellMenuDirective,
    ShellContentDirective,
    ShellToolDirective
  ],
  exports: [
    ShellComponent,
    ShellMenuDirective,
    ShellContentDirective,
    ShellToolDirective
  ]
})
export class ShellModule { }
