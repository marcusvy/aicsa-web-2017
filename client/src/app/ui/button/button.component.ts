import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'button[mv-btn]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ButtonComponent implements OnInit {

  positioned;
  onTop;
  onLeft;
  onRight;
  onBottom;

  constructor() { }

  ngOnInit() {
  }

}
