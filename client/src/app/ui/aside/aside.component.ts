import { Component, OnInit, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'mv-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit {

  @Input() title = '';

  constructor() { }

  ngOnInit() {
  }

  @HostBinding('class.js-mv-aside--opened') opened = false;

  onToggle() {
    this.opened = !this.opened;
  }
  onOpen() {
    this.opened = true;
  }
  onClose() {
    this.opened = false;
  }

}
