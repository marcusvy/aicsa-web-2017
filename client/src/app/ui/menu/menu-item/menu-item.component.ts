import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'mv-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MenuItemComponent implements OnInit {

  private hasLink: Boolean = false;

  @Input() href: string|null = null;
  @Input() icon: string;
  @Output() onMenuItemClick: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  constructor() { }

  ngOnInit() {

  }

  public get hasIcon(){
    return (this.icon.length > 0);
  }

  onClick($event) {
    this.onMenuItemClick.emit(true);
  }
}
