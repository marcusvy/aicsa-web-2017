import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from "../layout/layout.module";

import { FooterComponent } from './footer.component';
import { FooterAboutDirective } from './footer-about.directive';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
  ],
  declarations: [
    FooterComponent,
    FooterAboutDirective,
  ],
  exports: [
    FooterComponent,
    FooterAboutDirective,
  ],
})
export class FooterModule { }
