import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validator} from '@angular/forms';
import { Pagination } from '../pagination';
import { PaginationService } from '../pagination.service';
import { Subscription } from 'rxjs/Rx';
@Component({
  selector: 'mv-pagination-control',
  templateUrl: './pagination-control.component.html',
  styleUrls: ['./pagination-control.component.scss'],
})
export class PaginationControlComponent implements OnInit, OnDestroy {

  // @Input() limit: number = 5;
  @ViewChild('modal') modal;
  pagination: Pagination; //result
  subscription: Subscription;
  formPagination: FormGroup;

  @Output() onPagination: EventEmitter<boolean> = new EventEmitter<boolean>();
  // total: number;
  // current: number = 1;
  // pages: number = 1;
  // limitLeft: number;
  // limitRight: number;


  constructor(
    private fb: FormBuilder,
    private service: PaginationService
  ) { }

  ngOnInit() {
    this.formBuilder(1, 5);
    this.subscription = this.service.getPagination()
      .subscribe((p: Pagination) => {
        this.pagination = p;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private formBuilder(current, limit) {
    this.formPagination = this.fb.group({
      page: [current],
      limit: [limit]
    });
  }

  getPageList() {
    return this.service.getPageList();
  }

  getLimitList() {
    // this.onPagination.emit(true);
    return this.service.getLimitList();
  }

  isCollectionEmpty() {
    return this.service.isCollectionEmpty();
  }

  isNext() {
    return this.service.isNext();
  }

  showNext() {
    return this.service.showNext();
  }

  isPrevious() {
    return this.service.isPrevious();
  }

  showPrevious() {
    return this.service.showPrevious();
  }

  isFirst() {
    return this.service.isFirst();
  }

  showFirst() {
    return this.service.showFirst();
  }

  isLast() {
    return this.service.isLast();
  }
  showLast() {
    return this.service.showLast();
  }

  onChangeLimit(limit) {
    this.service.changeLimit(limit);
  }

  goToPage(page) {
    this.service.goToPage(page);
  }

  goToNext() {
    this.service.goToNext();
  }

  goToPrevious() {
    this.service.goToPrevious();
  }

  goToFirst() {
    this.service.goToFirst();
  }

  goToLast() {
    this.service.goToLast();
  }
}
