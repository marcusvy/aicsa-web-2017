import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/Rx';

import { Pagination } from './pagination';


@Injectable()
export class PaginationService {

  private collection: any[] = [];
  private limit: number = 5;
  private total: number;
  private current: number = 1;
  private pages: number = 1;
  private limitLeft: number;
  private limitRight: number;

  private _pagination: BehaviorSubject<Pagination> = new BehaviorSubject({});

  constructor() { }

  /**
   * Configuration
   */

  public getCollection() {
    return this.collection;
  }
  public setCollection(collection: any[]) {
    this.collection = collection;
  }

  public getLimit() {
    return this.limit;
  }

  public setLimit(value) {
    this.limit = value;
    return this.limit;
  }

  public getTotal() {
    return this.total;
  }

  public setTotal(value) {
    this.total = value;
    return this.total;
  }

  public getCurrentPage() {
    return this.current;
  }

  public setCurrentPage(value) {
    this.current = value;
    return this.current;
  }

  public getPages() {
    return this.pages;
  }

  public setPages(value) {
    this.pages = value;
    return this.pages;
  }

  /**
   * Available lists
   */
  public getPageList() {
    let pageList = [];
    for (let i = 0; i < this.pages; i++) {
      pageList[i] = i + 1;
    }
    return pageList;
  }

  public getLimitList() {
    return [5, 10, 50, 100];
  }

  /**
   * Check
   */
  isCollectionEmpty() {
    return (this.collection.length === 0);
  }

  isNext() {
    return (this.current >= 1 && this.current < this.pages);
  }

  showNext() {
    return (this.current < this.pages && !this.isCollectionEmpty());
  }

  isPrevious() {
    return (this.current > 1 && this.current <= this.pages);
  }

  showPrevious() {
    return (this.current > 1 && !this.isCollectionEmpty());
  }

  isFirst() {
    return (this.current === 1);
  }

  showFirst() {
    return (this.current > 1 && !this.isCollectionEmpty())
  }

  isLast() {
    return (this.current === this.pages);
  }
  showLast() {
    return (this.current < this.pages && !this.isCollectionEmpty());
  }

  /**
   * Prepare for pagination
   */
  private calcTotal(): number {
    this.total = this.collection.length;
    return this.total;
  }
  private calcPages(): number {
    this.pages = Math.ceil(this.total / this.limit);
    return this.pages;
  }
  private calcLimitLeft(): number {
    this.limitLeft = ((this.limit * this.current) - this.limit);
    return this.limitLeft;
  }
  private calcLimitRight(): number {
    this.limitRight = this.limitLeft + this.limit;
    return this.limitRight;
  }

  private calc() {
    this.calcTotal();
    this.calcPages();
    this.calcLimitLeft();
    this.calcLimitRight();
  }

  /**
 * Execution
 */
  public getPagination(): Observable<Pagination> {
    return this._pagination.asObservable();
  }

  public getPaginatedData() {
    return this._pagination.getValue();
  }

  private getPaginationInstance() {
    return {
      limit: this.limit,
      current: this.current,
      total: this.total,
      pages: this.pages,
      limitLeft: this.limitLeft,
      limitRight: this.limitRight,
      collection: this.collection,
    };
  }

  public config(pagination: Pagination) {
    if (this.isCollectionEmpty() || pagination.collection) {
      this.setCollection(pagination.collection);
    }
    if (pagination.current) {
      this.setCurrentPage(pagination.current);
    }
    if (pagination.limit) {
      this.setLimit(pagination.limit);
    }
    this.calc();
    return this._pagination.next(Object.assign({}, this.getPaginationInstance(), pagination));
  }

  paginate() {
    let page = {
      limit: this.limit,
      current: this.current,
      total: this.calcTotal(),
      pages: this.calcPages(),
      limitLeft: this.calcLimitLeft(),
      limitRight: this.calcLimitRight(),
      collection: this.collection,
    };
    this._pagination.next(page);
    return page;
  }
  /**
   * Events
   */
  changeLimit(limit) {
    if (typeof limit === 'string') {
      limit = Number.parseInt(limit);
    }
    this.limit = limit;
    this.current = 1;
    this.paginate();
  }
  goToPage(page) {
    this.current = page;
    this.paginate();
  }
  goToNext() {
    if (this.isNext()) {
      this.current++;
    }
    this.paginate();
  }
  goToPrevious() {
    if (this.isPrevious()) {
      this.current--;
    }
    this.paginate();
  }
  goToFirst() {
    this.current = 1;
    this.paginate();
  }

  goToLast() {
    this.current = this.pages;
    this.paginate();
  }
}
