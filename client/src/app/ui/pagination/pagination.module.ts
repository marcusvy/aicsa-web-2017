import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LayoutModule } from "../layout/layout.module";
import { ButtonModule } from "../button/button.module";
import { ControlModule } from '../control/control.module';
import { IconModule } from "../icon/icon.module";
import { MenuModule } from "../menu/menu.module";
import { ModalModule } from "../modal/modal.module";


import { PaginationPipe } from './pagination.pipe';
import { PaginationControlComponent } from './pagination-control/pagination-control.component';
import { PaginationService } from './pagination.service';
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    LayoutModule,
    ButtonModule,
    ControlModule,
    IconModule,
    MenuModule,
    ModalModule,
  ],
  declarations: [
    PaginationPipe,
    PaginationControlComponent,
  ],
  exports: [
    PaginationPipe,
    PaginationControlComponent,
  ],
  providers: [
    PaginationService
  ]
})
export class PaginationModule { }
