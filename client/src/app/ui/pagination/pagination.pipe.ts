import { OnChanges, OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { Pagination } from './pagination';
import { PaginationService } from './pagination.service';
import { Observable, Subscription } from 'rxjs/Rx';
@Pipe({
  name: 'mvPagination',
  pure: false
})
export class PaginationPipe implements PipeTransform, OnChanges, OnDestroy {

  private result = [];
  private subscription: Subscription;

  constructor(private service: PaginationService) { }

  transform(collection: any[], limit: number = 5) {
    // Configura serviço para paginar
    this.service.config({
      collection: collection,
      limit: limit
    });

    // Pagina o coleção
    // this.subscription = this.service.getPagination()
    //   .map((res:any)=>res.json())
    //   .subscribe((p) => {
    //     this.result = p.collection.slice(p.limitLeft, p.limitRight);
    //   });
    return this.service.getPagination();

    // return this.result;
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }

  ngOnChanges() {
    this.service.paginate();
  }
}
