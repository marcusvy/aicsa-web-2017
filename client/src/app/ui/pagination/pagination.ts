export interface Pagination {
  limit?: number;
  total?: number;
  current?: number;
  pages?: number;
  limitLeft?: number;
  limitRight?: number;
  collection?: any[];
}
