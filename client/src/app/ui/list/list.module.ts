import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from "../layout/layout.module";

import { ListComponent } from './list.component';
import { ListItemComponent } from './list-item/list-item.component';
import { ListItemPrimaryDirective } from './list-item/list-item-primary.directive';
import { ListItemSecondaryDirective } from './list-item/list-item-secondary.directive';
import { ListItemActionDirective } from './list-item/list-item-action.directive';
import { ListDivisorDirective } from './list-divisor.directive';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
  ],
  declarations: [
    ListComponent,
    ListDivisorDirective,
    ListItemComponent,
    ListItemPrimaryDirective,
    ListItemSecondaryDirective,
    ListItemActionDirective,
  ],
  exports: [
    ListComponent,
    ListDivisorDirective,
    ListItemComponent,
    ListItemPrimaryDirective,
    ListItemSecondaryDirective,
    ListItemActionDirective,
  ],
})
export class ListModule { }
