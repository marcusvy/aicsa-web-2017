import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  HostBinding,
  HostListener,
  ViewEncapsulation
} from '@angular/core';

import {
  animate,
  trigger,
  state,
  transition,
  style
} from '@angular/animations';

@Component({
  selector: 'mv-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('modalAnimation', [
      state('opened', style({ display: 'flex', opacity: 1 })),
      state('closed', style({ display: 'none', opacity: 0 })),
      transition('opened <=> closed', animate(300)),
    ]),
    trigger('modalBoxAnimation', [
      state('opened', style({ transform: 'scale(1)', opacity: 1 })),
      state('closed', style({ transform: 'scale(0)', opacity: 0 })),
      transition('opened <=> closed', animate(300)),
    ]),
    trigger('modalBackdropAnimation', [
      state('show', style({ opacity: 1 })),
      state('hide', style({ opacity: 0 })),
      transition('show <=> hide', animate(500)),
    ]),
  ],
})
export class ModalComponent implements OnInit, OnDestroy {

  @Input() title: string = '';
  @Input() disableBackdrop: boolean = false;
  @Output() onClose: EventEmitter<any> = new EventEmitter();


  private visible: Boolean = false;
  private escKeyListener;

  @HostBinding('@modalAnimation') get state() {
    return (this.visible) ? 'opened' : 'closed';
  };

  constructor() { }

  ngOnInit() {

  }

  ngOnDestroy() {

  }

  close(){
    this.visible = false;
    this.onClose.emit(true);
  }

  open() {
    this.visible = true;
  }

  onBackdropClick() {
    if (!this.disableBackdrop) {
      this.close();
    }
  }
}
