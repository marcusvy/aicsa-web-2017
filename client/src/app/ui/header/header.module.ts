import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from '../button/button.module';
import { LayoutModule } from '../layout/layout.module';
import { ToolbarModule } from '../toolbar/toolbar.module';

import { HeaderComponent } from './header.component';
import { HeaderMenuDirective } from './header-menu.directive';
import { HeaderLogoDirective } from './header-logo.directive';
import { HeaderToolDirective } from './header-tool.directive';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    LayoutModule,
    ToolbarModule,
  ],
  declarations: [
    HeaderComponent,
    HeaderMenuDirective,
    HeaderLogoDirective,
    HeaderToolDirective,
  ],
  exports: [
    HeaderComponent,
    HeaderMenuDirective,
    HeaderLogoDirective,
    HeaderToolDirective,
  ],
})
export class HeaderModule { }
