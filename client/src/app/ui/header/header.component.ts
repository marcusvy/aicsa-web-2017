import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'mv-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() image: string = '';
  @Input() title: string = '';
  @Input() subtitle: string = '';
  @Input() button: string = '';
  @Input() url: string = '';
  @Input() filter: Boolean = false;
  @Input() filterColor: String = '';
  @Input() noLogo: Boolean = false;
  @Output() onButtonClicked = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  hasContent() {
    return ( this.title.length > 0) ||
           ( this.subtitle.length > 0) ||
           ( this.button.length > 0);
  }

  hasImage() {
    return (this.image.length > 0);
  }

  hasButton() {
    return (this.button.length > 0);
  }

  getHeaderClass() {
    let classes = {
      'mv-header--content' : this.hasContent() && !this.hasImage(),
      'mv-header--with-image' : this.hasImage(),
      'mv-header--with-filter' : this.filter,
      'mv-header--no-logo' : this.noLogo
    };
    return classes;
  }

  getHeaderStyle() {
    let style = {
      'background-image': `url('${this.image}')`
    };
    return style;
  }

  getFilterStyle() {
    let hasFilterColor = (this.filterColor.length > 0);
    let style = hasFilterColor ? {
      'background-color' : this.filterColor,
    } : {};
    if (hasFilterColor) {
      this.filter = true;
    }
    return style;
  }

  onButtonClick($event) {
    this.onButtonClicked.emit(this.url);
  }

}
