import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from '../button/button.module';
import { IconModule } from '../icon/icon.module';
import { LayoutModule } from '../layout/layout.module';

import { ControlComponent } from './control.component';
import { ControlActionDirective } from './control-action.directive';
import { ControlSetupDirective } from './control-setup.directive';
import { ControlStatusDirective } from './control-status.directive';
import { ControlTrueDirective } from './control-true.directive';
import { ControlFalseDirective } from './control-false.directive';
import { ControlCheckboxDirective } from './control-checkbox.directive';
import { ControlNumberComponent } from './control-number/control-number.component';
import { ControlReactiveComponent } from './control-reactive/control-reactive.component';
import { ControlTextComponent } from './control-text/control-text.component';
import { ControlErrorComponent } from './control-error/control-error.component';
import { ControlMessageDirective } from './control-message.directive';
import { ControlErrorsDirective } from './control-errors.directive';
import { ControlValidatorService } from './control-validator.service';
import { ControlMaskDirective } from './control-mask.directive';
import { ControlEditorComponent } from './control-editor/control-editor.component';
import { ControlEditorAreaDirective } from './control-editor/control-editor-area.directive';
import { ControlCheckboxComponent } from './control-checkbox/control-checkbox.component';
import { ControlSwitchComponent } from './control-switch/control-switch.component';
import { ControlSelectComponent } from './control-select/control-select.component';
import { ControlFileComponent } from './control-file/control-file.component';
import { ControlContainerComponent } from './control-container/control-container.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    IconModule,
    LayoutModule,
  ],
  declarations: [
    ControlComponent,
    ControlEditorAreaDirective,
    ControlActionDirective,
    ControlSetupDirective,
    ControlStatusDirective,
    ControlTrueDirective,
    ControlFalseDirective,
    ControlCheckboxDirective,
    ControlNumberComponent,
    ControlReactiveComponent,
    ControlTextComponent,
    ControlErrorComponent,
    ControlErrorsDirective,
    ControlMessageDirective,
    ControlMaskDirective,
    ControlEditorComponent,
    ControlCheckboxComponent,
    ControlSwitchComponent,
    ControlSelectComponent,
    ControlFileComponent,
    ControlContainerComponent,
  ],
  exports: [
    ControlComponent,
    ControlActionDirective,
    ControlSetupDirective,
    ControlStatusDirective,
    ControlTrueDirective,
    ControlFalseDirective,
    ControlNumberComponent,
    ControlTextComponent,
    ControlErrorComponent,
    ControlErrorsDirective,
    ControlMessageDirective,
    ControlMaskDirective,
    ControlEditorComponent,
    ControlCheckboxComponent,
    ControlSwitchComponent,
    ControlSelectComponent,
    ControlFileComponent,
    ControlContainerComponent,
  ],
  providers: [
    ControlValidatorService
  ]
})
export class ControlModule { }
