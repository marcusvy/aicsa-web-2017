/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {CoreModule} from '../../core/core.module';
import {ControlReactiveComponent} from '../control-reactive/control-reactive.component';
import {ControlTextComponent} from './control-text.component';
import { ControlMessageDirective } from '../control-message.directive';

describe('ControlTextComponent', () => {
  let component: ControlTextComponent;
  let fixture: ComponentFixture<ControlTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ CoreModule ],
      declarations: [
        ControlReactiveComponent,
        ControlTextComponent,
        ControlMessageDirective,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
