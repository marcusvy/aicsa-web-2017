import { Component, OnInit, Input, forwardRef, ViewChild, ContentChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RandomIdGenerator } from '../../../core/util/random-id-generator';
import { ControlErrorsDirective } from "../control-errors.directive";
import { BoletoPipe } from "../../../core/pipe/boleto.pipe";
import { CepPipe } from "../../../core/pipe/cep.pipe";
import { CnpjPipe } from "../../../core/pipe/cnpj.pipe";
import { CpfPipe } from "../../../core/pipe/cpf.pipe";
import { PhonePipe } from "../../../core/pipe/phone.pipe";

@Component({
  selector: 'mv-control-text',
  templateUrl: './control-text.component.html',
  styleUrls: ['./control-text.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ControlTextComponent),
    multi: true
  }],
})
export class ControlTextComponent implements OnInit, ControlValueAccessor {

  @Input() formControl;
  @Input() _id: string = '';
  @Input() type: string = 'text';
  @Input() alt: string;
  @Input() autofocus: string;
  @Input() max: string;
  @Input() maxlength: string;
  @Input() min: string;
  @Input() name: string;
  @Input() pattern: string;
  @Input() placeholder: string;
  @Input() readonly: string;
  @Input() size: string;
  @Input() _value: any = '';
  @Input() label: string;
  @Input() mask: string;
  private maskPipe: any;

  @ContentChild(ControlErrorsDirective)
  private controlErrors: ControlErrorsDirective;

  onChange = (_: any) => {
  };
  onTouched = (_: any) => {
  };

  constructor() {
  }

  get id() {
    if (this._id === '') {
      let generator = new RandomIdGenerator();
      this._id = generator.random;
    }
    return this._id;
  }

  ngOnInit() {
    //Registra formcontrol em mv-control-errors
    if (this.controlErrors !== undefined) {
      this.controlErrors.registerFormControl(this.formControl);
    }
    this.registerMask();
  }

  get value() {
    return this._value;
  }

  set value(obj) {
    this._value = obj;
  }

  /* Control Events */
  onInput($event) {
    let value = $event.target.value;
    if (this.maskPipe !== undefined && this.maskPipe.__proto__.hasOwnProperty('parse')) {
      $event.target.value = this.maskPipe.transform(value);
      value = this.maskPipe.parse(value);
    }
    this.onChange(value);
  }

  onFocus($event) {
    let value = $event.target.value;
    if (this.maskPipe !== undefined && this.maskPipe.__proto__.hasOwnProperty('transform')) {
      $event.target.value = this.maskPipe.transform(value);
    }
  }

  onBlur($event) {
    let value = $event.target.value;
    if (this.maskPipe !== undefined && this.maskPipe.__proto__.hasOwnProperty('transform')) {
      $event.target.value = this.maskPipe.transform(value);
      value = this.maskPipe.parse(value);
    }
    this.onTouched(value);
  }

  /* ControlValueAccessor */
  writeValue(obj: any): void {
    if (obj !== undefined) {
      this.value = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  /**
   * Mask
   */
  registerMask(): boolean {
    if (this.mask !== undefined) {
      switch (this.mask) {
        case 'boleto':
          this.maskPipe = new BoletoPipe();
          break;
        case 'cep':
          this.maskPipe = new CepPipe();
          break;
        case 'cnpj':
          this.maskPipe = new CnpjPipe();
          break;
        case 'cpf':
          this.maskPipe = new CpfPipe();
          break;
        case 'phone':
          this.maskPipe = new PhonePipe();
          break;
        default:
          this.maskPipe = this.mask;
          break;
      }
    }
    return (this.maskPipe !== undefined);
  }

}

