import {Component, OnInit, HostBinding, Input} from '@angular/core';

@Component({
  selector: 'mv-control-error',
  templateUrl: './control-error.component.html',
  styleUrls: [ './control-error.component.scss' ]
})
export class ControlErrorComponent implements OnInit {

  @HostBinding('style.display') get display(){
    return (this._visible) ? 'list-item' : 'none';
  }

  @Input() validator:string;

  private _visible:boolean = false;

  get visible(){
    return this._visible;
  }
  set visible(value:boolean){
    this._visible = value;
  }

  constructor() {}

  ngOnInit() {

  }

  show(){
    this.visible = true;
  }

  hide(){
    this.visible = false;
  }
}
