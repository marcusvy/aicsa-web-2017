import { Component, OnInit, Input, forwardRef, ViewChild, ContentChild, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RandomIdGenerator } from '../../../core/util/random-id-generator';
import { ControlErrorsDirective } from "../control-errors.directive";


@Component({
  selector: 'mv-control-checkbox',
  templateUrl: './control-checkbox.component.html',
  styleUrls: ['./control-checkbox.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ControlCheckboxComponent),
    multi: true
  }],
})
export class ControlCheckboxComponent implements OnInit {

  @Input() formControl;
  @Input() label: string;
  @Input() _id: string = '';
  @Input() alt: string;
  @Input() autofocus: string;
  @Input() name: string;
  @Input() readonly: string;
  @Input('value') _value: boolean = false;

  @ContentChild(ControlErrorsDirective)
  private controlErrors: ControlErrorsDirective;

  onChange = (_: any) => {
  };
  onTouched = (_: any) => {
  };

  constructor() {
  }

  get id() {
    if (this._id === '') {
      let generator = new RandomIdGenerator();
      this._id = generator.random;
    }
    return this._id;
  }

  ngOnInit() {
    //Registra formcontrol em mv-control-errors
    if (this.controlErrors !== undefined) {
      this.controlErrors.registerFormControl(this.formControl);
    }
  }

  get value() {
    return this._value;
  }

  set value(obj) {
    this._value = obj;
  }

  /* Control Events */
  onCheckboxChange($event) {
    let value = $event.target.checked;
    this.value = value;
    this.onChange(value);
  }

  onBlur($event) {
    let value = $event.target.checked;
    this.value = value;
    this.onTouched(value);
  }

  /* ControlValueAccessor */
  writeValue(obj: any): void {
    if (obj !== undefined) {
      this.value = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

}
