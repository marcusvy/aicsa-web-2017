import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewEncapsulation,
  ViewChild,
  Output,
  Renderer
} from '@angular/core';
import {NG_VALUE_ACCESSOR, ControlValueAccessor} from '@angular/forms';
import {ControlEditorAreaDirective} from "./control-editor-area.directive";

import * as Quill from 'quill';

@Component({
  selector: 'mv-control-editor',
  templateUrl: './control-editor.component.html',
  styleUrls: ['./control-editor.component.scss'],
  providers: [ {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ControlEditorComponent),
    multi: true
  } ],
  encapsulation: ViewEncapsulation.None,
})
export class ControlEditorComponent implements AfterViewInit, ControlValueAccessor, OnChanges {

  @Input() formControl;
  @Input() label:string = '';
  @Input() icon:string = '';
  @Input() placeholder:string = '...';
  @Input() readonly:boolean = false;
  @Input() theme:string = 'snow'; //snow, core, bubble
  @Input() config: Object;

  @Output() ready: EventEmitter<any> = new EventEmitter();
  @Output() change: EventEmitter<any> = new EventEmitter();
  @ViewChild('controlEditorArea') quillElement: ElementRef;

  private value: any;
  private quillEditor: any;
  private defaultModules = {
    toolbar: [
      [ 'bold', 'italic', 'underline', 'strike' ],        // toggled buttons
      [ 'blockquote', 'code-block' ],

      [ {'header': 1}, {'header': 2} ],               // custom button values
      [ {'list': 'ordered'}, {'list': 'bullet'} ],
      [ {'script': 'sub'}, {'script': 'super'} ],      // superscript/subscript
      [ {'indent': '-1'}, {'indent': '+1'} ],          // outdent/indent
      [ {'direction': 'rtl'} ],                         // text direction

      [ {'size': [ 'small', false, 'large', 'huge' ]} ],  // custom dropdown
      [ {'header': [ 1, 2, 3, 4, 5, 6, false ]} ],

      [ {'color': []}, {'background': []} ],          // dropdown with defaults from theme
      [ {'font': ['Arial']} ],
      [ {'align': []} ],

      [ 'clean' ],                                         // remove formatting button

      [ 'link', 'image', 'video' ]                         // link and image, video
    ],
  };

  onChange: Function = () => {};
  onTouched: Function = () => {};

  constructor(private renderer:Renderer) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    const quillElement = this.quillElement.nativeElement;

    this.quillEditor = new Quill(quillElement, Object.assign({
      modules: this.defaultModules,
      placeholder: this.placeholder,
      readOnly: this.readonly,
      theme: this.theme,
      boundary: document.body
    }, this.config || {}));

    // write
    if (this.value) {
      this.quillEditor.pasteHTML(this.value);
    }

    // events
    this.ready.emit(this.quillEditor);

    // mark model as touched if editor lost focus
    this.quillEditor.on('selection-change', (range) => {
      if (!range) {
        this.onTouched();
      }
    });

    // update model if text changes
    this.quillEditor.on('text-change', (delta, oldDelta, source) => {
      let html = quillElement.children[0].innerHTML;
      const text = this.quillEditor.getText();

      if (html === '<p><br></p>') html = null;

      this.onChange(html);

      this.change.emit({
        editor: this.quillEditor,
        html: html,
        text: text
      });
    });

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['readOnly'] && this.quillEditor) {
      this.quillEditor.enable(!changes['readOnly'].currentValue);
    }
  }

  /* ControlValueAccessor */
  writeValue(obj: any): void {
    if (obj !== undefined) {
      this.value = obj;
    }

    if (this.quillEditor) {
      if (obj) {
        this.quillEditor.pasteHTML(obj);
        return;
      }
      this.quillEditor.setText('');
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }


  onLabelClick($event) {
    let quillElementEditor = this.quillElement.nativeElement.children[0];
    this.renderer.invokeElementMethod(quillElementEditor,'focus');
  }

}
