import {Directive, Input, ContentChildren, QueryList, DoCheck} from '@angular/core';
import {ControlErrorComponent} from "./control-error/control-error.component";

@Directive({
  selector: 'mv-control-errors'
})
export class ControlErrorsDirective implements DoCheck {

  private _formControl;
  @ContentChildren(ControlErrorComponent) errors: QueryList<ControlErrorComponent>;

  get formControl() {
    return this._formControl;
  }

  set formControl(newValue) {
    this._formControl = newValue;
  }

  constructor() {
  }

  ngDoCheck() {
    if (this.errors !== undefined) {
      this.errors.toArray().forEach((error: ControlErrorComponent) => {
        error.hide();
        if (!this._formControl.pristine && this.formControl.errors) {
          if (this.formControl.errors.hasOwnProperty(error.validator)) {
            error.show();
          }
        }
      });
    }
  }

  registerFormControl(formControl) {
    this._formControl = formControl;
  }

}
