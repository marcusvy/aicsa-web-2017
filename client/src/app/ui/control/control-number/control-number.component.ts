import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RandomIdGenerator } from '../../../core/util/random-id-generator';

@Component({
  selector: 'mv-control-number',
  templateUrl: './control-number.component.html',
  styleUrls: ['./control-number.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ControlNumberComponent),
    multi: true
  }]
})
export class ControlNumberComponent implements OnInit, ControlValueAccessor {

  @Input() _inputId: string = '';
  @Input() _counterValue = 0;
  onChange = (_: any) => {
  };
  onTouched = (_: any) => {
  };

  constructor() {
  }

  get inputId() {
    if (this._inputId === '') {
      let generator = new RandomIdGenerator();
      this._inputId = generator.random;
    }
    return this._inputId;
  }

  get counterValue() {
    return this._counterValue;
  }

  set counterValue(value) {
    this._counterValue = value;
    this.onChange(this._counterValue);
  }

  /** Implementations */
  ngOnInit() {
  }

  writeValue(value: any): void {
    if (value !== undefined) {
      this.counterValue = value;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  /**
   * Custom
   */
  increment() {
    this.counterValue++;
  }

  decrement() {
    this.counterValue--;
  }
}
