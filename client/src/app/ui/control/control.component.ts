import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-control',
  template: '',
  styleUrls: ['./control.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ControlComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }
}
