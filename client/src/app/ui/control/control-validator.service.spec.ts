/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ControlValidatorService } from './control-validator.service';

describe('ControlValidatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ControlValidatorService]
    });
  });

  it('should ...', inject([ControlValidatorService], (service: ControlValidatorService) => {
    expect(service).toBeTruthy();
  }));
});
