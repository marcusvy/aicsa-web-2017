import { Component, OnInit, Input, forwardRef, ViewChild, ContentChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RandomIdGenerator } from '../../../core/util/random-id-generator';
import { ControlErrorsDirective } from "../control-errors.directive";


@Component({
  selector: 'mv-control-select',
  templateUrl: './control-select.component.html',
  styleUrls: ['./control-select.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ControlSelectComponent),
    multi: true
  }],
})
export class ControlSelectComponent implements OnInit, ControlValueAccessor {

  @Input() formControl;
  @Input() _id: string = '';
  @Input() autofocus: string;
  @Input() disabled: boolean;
  @Input() form: string;
  @Input() multiple: boolean;
  @Input() name: string;
  @Input() required: boolean;
  @Input() size: string;
  @Input() _value: any = '';
  @Input() label: string;

  @ContentChild(ControlErrorsDirective)
  private controlErrors: ControlErrorsDirective;

  onChange = (_: any) => {
  };
  onTouched = (_: any) => {
  };

  constructor() {
  }

  get id() {
    if (this._id === '') {
      let generator = new RandomIdGenerator();
      this._id = generator.random;
    }
    return this._id;
  }

  ngOnInit() {
    //Registra formcontrol em mv-control-errors
    if (this.controlErrors !== undefined) {
      this.controlErrors.registerFormControl(this.formControl);
    }
  }

  get value() {
    return this._value;
  }

  set value(obj) {
    this._value = obj;
  }

  /* Control Events */
  onSelect($event) {
    let value = $event.target.value;
    this.onChange(value);
  }

  onBlur($event) {
    let value = $event.target.value;
    this.onTouched(value);
  }

  /* ControlValueAccessor */
  writeValue(obj: any): void {
    if (obj !== undefined) {
      this.value = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

}
