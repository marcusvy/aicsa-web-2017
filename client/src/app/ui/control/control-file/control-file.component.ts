import { Component, OnInit, Input, forwardRef, ViewChild, ContentChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RandomIdGenerator } from '../../../core/util/random-id-generator';
import { ControlErrorsDirective } from "../control-errors.directive";
// import {BoletoPipe} from "../../../core/pipe/boleto.pipe";
// import {CepPipe} from "../../../core/pipe/cep.pipe";
// import {CnpjPipe} from "../../../core/pipe/cnpj.pipe";
// import {CpfPipe} from "../../../core/pipe/cpf.pipe";
// import {PhonePipe} from "../../../core/pipe/phone.pipe";


@Component({
  selector: 'mv-control-file',
  templateUrl: './control-file.component.html',
  styleUrls: ['./control-file.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ControlFileComponent),
    multi: true
  }],
})
export class ControlFileComponent implements OnInit, ControlValueAccessor {


  @Input() formControl;
  @Input() _id: string = '';
  @Input() alt: string = '';
  @Input() autofocus: string;
  @Input() name: string;
  @Input() pattern: string;
  @Input() placeholder: string;
  @Input() readonly: string;
  @Input() size: string;
  @Input() _value: any = '';
  @Input() label: string;

  @ContentChild(ControlErrorsDirective)
  private controlErrors: ControlErrorsDirective;

  onChange = (_: any) => {
  };
  onTouched = (_: any) => {
  };

  constructor() {
  }

  get id() {
    if (this._id === '') {
      let generator = new RandomIdGenerator();
      this._id = generator.random;
    }
    return this._id;
  }

  ngOnInit() {
    //Registra formcontrol em mv-control-errors
    if (this.controlErrors !== undefined) {
      this.controlErrors.registerFormControl(this.formControl);
    }
  }

  get value() {
    return this._value;
  }

  set value(obj) {
    this._value = obj;
  }

  /* Control Events */
  onChoose($event) {
    let value = $event.srcElement.files;
    console.log($event);
    this.onChange(value);
  }

  onFocus($event) {
    let value = $event.srcElement.files;
  }

  onBlur($event) {
    let value = $event.srcElement.files;
    this.onTouched(value);
  }

  /* ControlValueAccessor */
  writeValue(obj: any): void {
    if (obj !== undefined) {
      this.value = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

}
