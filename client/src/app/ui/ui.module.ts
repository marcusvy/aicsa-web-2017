import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/**
 * Ui Modules
 */
import { AvatarModule } from './avatar/avatar.module';
import { AsideModule } from './aside/aside.module';
import { BadgeModule } from './badge/badge.module';
import { ButtonModule } from './button/button.module';
import { CardModule } from './card/card.module';
import { ControlModule } from './control/control.module';
import { DeviceModule } from './device/device.module';
import { FooterModule } from './footer/footer.module';
import { HeaderModule } from './header/header.module';
import { IconModule } from './icon/icon.module';
import { LayoutModule } from './layout/layout.module';
import { ListModule } from './list/list.module';
import { MenuModule } from './menu/menu.module';
import { ModalModule } from './modal/modal.module';
import { PageTitleModule } from './page-title/page-title.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { ToolbarModule } from './toolbar/toolbar.module';
import { DataModule } from './data/data.module';
import { PaginationModule } from './pagination/pagination.module';
import { ShellModule } from './shell/shell.module';

/**
 * Ui component
 */
import { UiComponent } from './ui.component';
import { PagerPipe } from './pager.pipe';


const listUiModules = [
  AvatarModule,
  BadgeModule,
  ButtonModule,
  CardModule,
  ControlModule,
  FooterModule,
  HeaderModule,
  IconModule,
  LayoutModule,
  ListModule,
  MenuModule,
  ModalModule,
  PageTitleModule,
  SidebarModule,
  ToolbarModule,
  DataModule,
  PaginationModule,
  ShellModule
];

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AsideModule,
    AvatarModule,
    BadgeModule,
    ButtonModule,
    CardModule,
    ControlModule,
    DataModule,
    DeviceModule,
    FooterModule,
    HeaderModule,
    IconModule,
    LayoutModule,
    ListModule,
    MenuModule,
    ModalModule,
    PageTitleModule,
    PaginationModule,
    ShellModule,
    SidebarModule,
    ToolbarModule,
  ],
  declarations: [
    UiComponent,
    PagerPipe,
  ],
  exports: [
    AsideModule,
    AvatarModule,
    BadgeModule,
    ButtonModule,
    CardModule,
    ControlModule,
    DataModule,
    DeviceModule,
    FooterModule,
    HeaderModule,
    IconModule,
    LayoutModule,
    ListModule,
    MenuModule,
    ModalModule,
    PageTitleModule,
    PaginationModule,
    ShellModule,
    SidebarModule,
    ToolbarModule,
    UiComponent,
  ]
})
export class UiModule {
}
