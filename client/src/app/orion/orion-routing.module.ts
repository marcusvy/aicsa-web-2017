import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import {OrionComponent} from "./orion.component";
import {OrionDashboardComponent} from "./orion-dashboard/orion-dashboard.component";

const routes: Routes = [
  {
    path: 'orion',
    component: OrionComponent,
    children: [
      {path: '', component: OrionDashboardComponent, pathMatch: 'full'},
      {path: 'auth', loadChildren: '../orion-auth/orion-auth.module#OrionAuthModule'},
      {path: 'user-module', redirectTo: 'user-module/user'},
      {path: 'user-module', loadChildren: '../orion-user/orion-user.module#OrionUserModule'},
      // {path: 'sgd-module', redirectTo:'sgd-module/sgd'},
      // {path: 'sgd-module', loadChildren: './../sgd/sgd.module#SgdModule'},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ]
})
export class OrionRoutingModule {
}
