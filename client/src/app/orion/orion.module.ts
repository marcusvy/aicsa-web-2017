import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';

// import { SgdModule } from '../sgd/sgd.module';
import { AdunirModule } from '../adunir/adunir.module';
import { OrionAuthModule } from '../orion-auth/orion-auth.module';
import { OrionUserModule } from '../orion-user/orion-user.module';

import { OrionRoutingModule } from './orion-routing.module';

import { OrionComponent } from './orion.component';
import { OrionDashboardComponent } from './orion-dashboard/orion-dashboard.component';

@NgModule({
  imports: [
    CoreModule,
    // SgdModule,
    AdunirModule,
    OrionAuthModule,
    OrionUserModule,
    OrionRoutingModule,
  ],
  declarations: [
    OrionComponent,
    OrionDashboardComponent,
  ],
  exports: [
    OrionComponent
  ]
})
export class OrionModule { }
