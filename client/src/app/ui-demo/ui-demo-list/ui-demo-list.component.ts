import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mv-ui-demo-list',
  templateUrl: './ui-demo-list.component.html',
  styleUrls: ['./ui-demo-list.component.scss']
})
export class UiDemoListComponent implements OnInit {

  persons = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  constructor() { }

  ngOnInit() {
  }

}
