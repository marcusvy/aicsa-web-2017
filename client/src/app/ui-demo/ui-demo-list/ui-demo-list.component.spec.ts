import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoListComponent } from './ui-demo-list.component';

describe('UiDemoListComponent', () => {
  let component: UiDemoListComponent;
  let fixture: ComponentFixture<UiDemoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
