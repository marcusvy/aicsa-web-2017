import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoColorsComponent } from './ui-demo-colors.component';

describe('UiDemoColorsComponent', () => {
  let component: UiDemoColorsComponent;
  let fixture: ComponentFixture<UiDemoColorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoColorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoColorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
