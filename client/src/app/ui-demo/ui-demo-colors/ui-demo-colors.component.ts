import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mv-ui-demo-colors',
  templateUrl: './ui-demo-colors.component.html',
  styleUrls: ['./ui-demo-colors.component.scss']
})
export class UiDemoColorsComponent implements OnInit {

  palletes = [
    {
      name: 'primary', colors: [
        { variant: '50', value: '#e4e6e8' },
        { variant: '100', value: '#bcc2c6' },
        { variant: '200', value: '#9099a0' },
        { variant: '300', value: '#637079' },
        { variant: '400', value: '#41515d' },
        { variant: '500', value: '#203240' },
        { variant: '600', value: '#1c2d3a' },
        { variant: '700', value: '#182632' },
        { variant: '800', value: '#131f2a' },
        { variant: '900', value: '#0b131c' },
        { variant: 'A100', value: '#5da1ff' },
        { variant: 'A200', value: '#2a83ff' },
        { variant: 'A400', value: '#0067f6' },
        { variant: 'A700', value: '#005cdd' }
      ]
    },
    {
      name: 'secondary', colors: [
        { variant: '50', value: '#f6fcea' },
        { variant: '100', value: '#e9f8cb' },
        { variant: '200', value: '#daf3a8' },
        { variant: '300', value: '#cbee85' },
        { variant: '400', value: '#bfea6a' },
        { variant: '500', value: '#b4e650' },
        { variant: '600', value: '#ade349' },
        { variant: '700', value: '#a4df40' },
        { variant: '800', value: '#9cdb37' },
        { variant: '900', value: '#8cd527' },
        { variant: 'A100', value: '#f1ffdf' },
        { variant: 'A200', value: '#eaffd0' },
        { variant: 'A400', value: '#daffac' },
        { variant: 'A700', value: '#cfff92' }
      ]
    },
    {
      name: 'danger', colors: [
        { variant: '50', value: '#f5e7e2' },
        { variant: '100', value: '#e7c2b8' },
        { variant: '200', value: '#d79988' },
        { variant: '300', value: '#c67058' },
        { variant: '400', value: '#ba5235' },
        { variant: '500', value: '#ae3311' },
        { variant: '600', value: '#a72e0f' },
        { variant: '700', value: '#9d270c' },
        { variant: '800', value: '#94200a' },
        { variant: '900', value: '#841405' },
        { variant: 'A100', value: '#ffb8b2' },
        { variant: 'A200', value: '#ff887f' },
        { variant: 'A400', value: '#ff594c' },
        { variant: 'A700', value: '#ff4232' }
      ]
    },
    {
      name: 'warning', colors: [
        { variant: '50', value: '#fdf8e6' },
        { variant: '100', value: '#f9edc1' },
        { variant: '200', value: '#f6e197' },
        { variant: '300', value: '#f2d46d' },
        { variant: '400', value: '#efcb4e' },
        { variant: '500', value: '#ecc22f' },
        { variant: '600', value: '#eabc2a' },
        { variant: '700', value: '#e7b423' },
        { variant: '800', value: '#e4ac1d' },
        { variant: '900', value: '#df9f12' },
        { variant: 'A100', value: '#fff2da' },
        { variant: 'A200', value: '#ffe9c1' },
        { variant: 'A400', value: '#ffe1a7' },
        { variant: 'A700', value: '#ffd88d' }
      ]
    },
    {
      name: 'success', colors: [
        { variant: '50', value: '#e5ece6' },
        { variant: '100', value: '#becfc2' },
        { variant: '200', value: '#92b099' },
        { variant: '300', value: '#669070' },
        { variant: '400', value: '#467851' },
        { variant: '500', value: '#256032' },
        { variant: '600', value: '#21582d' },
        { variant: '700', value: '#1b4e26' },
        { variant: '800', value: '#16441f' },
        { variant: '900', value: '#0d3313' },
        { variant: 'A100', value: '#6fff82' },
        { variant: 'A200', value: '#3cff55' },
        { variant: 'A400', value: '#09ff29' },
        { variant: 'A700', value: '#00ef1f' }
      ]
    },
    {
      name: 'mono', colors: [
        { variant: 'light', value: '#ffffff' },
        { variant: '50', value: '#e7e7e7' },
        { variant: '100', value: '#c2c2c2' },
        { variant: '200', value: '#999999' },
        { variant: '300', value: '#707070' },
        { variant: '400', value: '#525252' },
        { variant: '500', value: '#333333' },
        { variant: '600', value: '#2e2e2e' },
        { variant: '700', value: '#272727' },
        { variant: '800', value: '#202020' },
        { variant: '900', value: '#141414' },
        { variant: 'dark', value: '#0A0A0A' }
      ]
    }
  ];

  constructor() { }

  ngOnInit() {

  }

}
