import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoAsideComponent } from './ui-demo-aside.component';

describe('UiDemoAsideComponent', () => {
  let component: UiDemoAsideComponent;
  let fixture: ComponentFixture<UiDemoAsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoAsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoAsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
