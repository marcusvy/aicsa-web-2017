import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiModule } from '../ui/ui.module';
import { UiDemoRoutingModule } from './ui-demo-routing.module';

import { UiDemoComponent } from './ui-demo.component';
import { UiDemoButtonComponent } from './ui-demo-button/ui-demo-button.component';
import { UiDemoColorsComponent } from './ui-demo-colors/ui-demo-colors.component';
import { UiDemoFormComponent } from './ui-demo-form/ui-demo-form.component';
import { UiDemoLayoutComponent } from './ui-demo-layout/ui-demo-layout.component';
import { UiDemoToolbarComponent } from './ui-demo-toolbar/ui-demo-toolbar.component';
import { UiDemoAsideComponent } from './ui-demo-aside/ui-demo-aside.component';
import { UiDemoAvatarComponent } from './ui-demo-avatar/ui-demo-avatar.component';
import { UiDemoBadgeComponent } from './ui-demo-badge/ui-demo-badge.component';
import { UiDemoCardComponent } from './ui-demo-card/ui-demo-card.component';
import { UiDemoDataComponent } from './ui-demo-data/ui-demo-data.component';
import { UiDemoDeviceComponent } from './ui-demo-device/ui-demo-device.component';
import { UiDemoFooterComponent } from './ui-demo-footer/ui-demo-footer.component';
import { UiDemoHeaderComponent } from './ui-demo-header/ui-demo-header.component';
import { UiDemoIconComponent } from './ui-demo-icon/ui-demo-icon.component';
import { UiDemoListComponent } from './ui-demo-list/ui-demo-list.component';
import { UiDemoMenuComponent } from './ui-demo-menu/ui-demo-menu.component';
import { UiDemoModalComponent } from './ui-demo-modal/ui-demo-modal.component';
import { UiDemoPaginationComponent } from './ui-demo-pagination/ui-demo-pagination.component';
import { UiDemoShellComponent } from './ui-demo-shell/ui-demo-shell.component';
import { UiDemoSidebarComponent } from './ui-demo-sidebar/ui-demo-sidebar.component';
import { UiDemoPagerComponent } from './ui-demo-pager/ui-demo-pager.component';


@NgModule({
  imports: [
    CommonModule,
    UiModule,
    UiDemoRoutingModule
  ],
  declarations: [
    UiDemoButtonComponent,
    UiDemoColorsComponent,
    UiDemoFormComponent,
    UiDemoLayoutComponent,
    UiDemoToolbarComponent,
    UiDemoAsideComponent,
    UiDemoAvatarComponent,
    UiDemoBadgeComponent,
    UiDemoCardComponent,
    UiDemoDataComponent,
    UiDemoDeviceComponent,
    UiDemoFooterComponent,
    UiDemoHeaderComponent,
    UiDemoIconComponent,
    UiDemoListComponent,
    UiDemoMenuComponent,
    UiDemoModalComponent,
    UiDemoPaginationComponent,
    UiDemoShellComponent,
    UiDemoSidebarComponent,
    UiDemoComponent,
    UiDemoPagerComponent
  ]
})
export class UiDemoModule { }
