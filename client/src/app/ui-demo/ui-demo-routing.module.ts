import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UiDemoComponent } from './ui-demo.component';

import { UiDemoButtonComponent } from './ui-demo-button/ui-demo-button.component';
import { UiDemoColorsComponent } from './ui-demo-colors/ui-demo-colors.component';
import { UiDemoFormComponent } from './ui-demo-form/ui-demo-form.component';
import { UiDemoLayoutComponent } from './ui-demo-layout/ui-demo-layout.component';
import { UiDemoToolbarComponent } from './ui-demo-toolbar/ui-demo-toolbar.component';
import { UiDemoAsideComponent } from './ui-demo-aside/ui-demo-aside.component';
import { UiDemoAvatarComponent } from './ui-demo-avatar/ui-demo-avatar.component';
import { UiDemoBadgeComponent } from './ui-demo-badge/ui-demo-badge.component';
import { UiDemoCardComponent } from './ui-demo-card/ui-demo-card.component';
import { UiDemoDataComponent } from './ui-demo-data/ui-demo-data.component';
import { UiDemoDeviceComponent } from './ui-demo-device/ui-demo-device.component';
import { UiDemoFooterComponent } from './ui-demo-footer/ui-demo-footer.component';
import { UiDemoHeaderComponent } from './ui-demo-header/ui-demo-header.component';
import { UiDemoIconComponent } from './ui-demo-icon/ui-demo-icon.component';
import { UiDemoListComponent } from './ui-demo-list/ui-demo-list.component';
import { UiDemoMenuComponent } from './ui-demo-menu/ui-demo-menu.component';
import { UiDemoModalComponent } from './ui-demo-modal/ui-demo-modal.component';
import { UiDemoPagerComponent } from './ui-demo-pager/ui-demo-pager.component';
import { UiDemoPaginationComponent } from './ui-demo-pagination/ui-demo-pagination.component';
import { UiDemoShellComponent } from './ui-demo-shell/ui-demo-shell.component';
import { UiDemoSidebarComponent } from './ui-demo-sidebar/ui-demo-sidebar.component';

const routes: Routes = [

  {
    path: 'demo',
    // pathMatch: 'full',
    component: UiDemoComponent,
    children: [
      { path: 'button', component: UiDemoButtonComponent },
      { path: 'colors', component: UiDemoColorsComponent },
      { path: 'form', component: UiDemoFormComponent },
      { path: 'layout', component: UiDemoLayoutComponent },
      { path: 'toolbar', component: UiDemoToolbarComponent },
      { path: 'aside', component: UiDemoAsideComponent },
      { path: 'avatar', component: UiDemoAvatarComponent },
      { path: 'badge', component: UiDemoBadgeComponent },
      { path: 'card', component: UiDemoCardComponent },
      { path: 'data', component: UiDemoDataComponent },
      { path: 'device', component: UiDemoDeviceComponent },
      { path: 'footer', component: UiDemoFooterComponent },
      { path: 'header', component: UiDemoHeaderComponent },
      { path: 'icon', component: UiDemoIconComponent },
      { path: 'list', component: UiDemoListComponent },
      { path: 'menu', component: UiDemoMenuComponent },
      { path: 'modal', component: UiDemoModalComponent },
      { path: 'pager', component: UiDemoPagerComponent },
      { path: 'pagination', component: UiDemoPaginationComponent },
      { path: 'shell', component: UiDemoShellComponent },
      { path: 'sidebar', component: UiDemoSidebarComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UiDemoRoutingModule { }
