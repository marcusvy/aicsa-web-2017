import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoDataComponent } from './ui-demo-data.component';

describe('UiDemoDataComponent', () => {
  let component: UiDemoDataComponent;
  let fixture: ComponentFixture<UiDemoDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
