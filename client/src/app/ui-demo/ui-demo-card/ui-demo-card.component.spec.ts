import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoCardComponent } from './ui-demo-card.component';

describe('UiDemoCardComponent', () => {
  let component: UiDemoCardComponent;
  let fixture: ComponentFixture<UiDemoCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
