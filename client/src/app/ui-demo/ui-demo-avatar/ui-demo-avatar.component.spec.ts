import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoAvatarComponent } from './ui-demo-avatar.component';

describe('UiDemoAvatarComponent', () => {
  let component: UiDemoAvatarComponent;
  let fixture: ComponentFixture<UiDemoAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
