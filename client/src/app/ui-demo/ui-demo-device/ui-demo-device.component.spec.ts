import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoDeviceComponent } from './ui-demo-device.component';

describe('UiDemoDeviceComponent', () => {
  let component: UiDemoDeviceComponent;
  let fixture: ComponentFixture<UiDemoDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
