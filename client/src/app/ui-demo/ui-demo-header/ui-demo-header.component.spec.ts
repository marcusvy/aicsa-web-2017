import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoHeaderComponent } from './ui-demo-header.component';

describe('UiDemoHeaderComponent', () => {
  let component: UiDemoHeaderComponent;
  let fixture: ComponentFixture<UiDemoHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
