import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mv-ui-demo-modal',
  templateUrl: './ui-demo-modal.component.html',
  styleUrls: ['./ui-demo-modal.component.scss']
})
export class UiDemoModalComponent implements OnInit {

  status = 'normal';

  constructor() { }

  ngOnInit() {
  }

  alertClose() {
    this.status = "fechou";
  }
}
