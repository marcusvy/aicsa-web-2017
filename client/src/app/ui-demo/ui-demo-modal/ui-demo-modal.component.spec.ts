import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoModalComponent } from './ui-demo-modal.component';

describe('UiDemoModalComponent', () => {
  let component: UiDemoModalComponent;
  let fixture: ComponentFixture<UiDemoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
