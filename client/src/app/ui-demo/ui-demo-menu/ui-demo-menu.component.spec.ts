import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoMenuComponent } from './ui-demo-menu.component';

describe('UiDemoMenuComponent', () => {
  let component: UiDemoMenuComponent;
  let fixture: ComponentFixture<UiDemoMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
