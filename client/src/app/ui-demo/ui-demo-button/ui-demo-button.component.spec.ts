import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoButtonComponent } from './ui-demo-button.component';

describe('UiDemoButtonComponent', () => {
  let component: UiDemoButtonComponent;
  let fixture: ComponentFixture<UiDemoButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
