import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoPaginationComponent } from './ui-demo-pagination.component';

describe('UiDemoPaginationComponent', () => {
  let component: UiDemoPaginationComponent;
  let fixture: ComponentFixture<UiDemoPaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoPaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
