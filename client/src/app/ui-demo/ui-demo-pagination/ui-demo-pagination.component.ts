import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';

import { PaginationService } from '../../ui/pagination';
import { ApiHttpService } from '../../core/http/api-http.service';

@Component({
  selector: 'mv-ui-demo-pagination',
  templateUrl: './ui-demo-pagination.component.html',
  styleUrls: ['./ui-demo-pagination.component.scss']
})
export class UiDemoPaginationComponent implements OnInit {

  private subscription: Subscription;
  private api;
  collection: any[] = [];
  page: any;

  constructor(
    private apiHttp: ApiHttpService,
    private pagination: PaginationService
  ) {
    //Configuração de Api
    this.api = this.apiHttp.factory('/api/user', 'mv-ui-user');
  }

  ngOnInit() {
    this.apiHttp.fetchAll().subscribe((result) => {
      this.collection = result.collection;
      this.pagination.config({
        collection: result.collection,
        // limit: limit
      });
    });

    this.subscription = this.pagination.getPagination()
      .subscribe((p) => {
        this.collection = p.collection.slice(p.limitLeft, p.limitRight);
      });
  }

  paginate(pagination) {

  }

}
