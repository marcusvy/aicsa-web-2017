import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoPagerComponent } from './ui-demo-pager.component';

describe('UiDemoPagerComponent', () => {
  let component: UiDemoPagerComponent;
  let fixture: ComponentFixture<UiDemoPagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoPagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoPagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
