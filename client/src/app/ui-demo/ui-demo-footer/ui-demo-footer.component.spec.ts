import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoFooterComponent } from './ui-demo-footer.component';

describe('UiDemoFooterComponent', () => {
  let component: UiDemoFooterComponent;
  let fixture: ComponentFixture<UiDemoFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
