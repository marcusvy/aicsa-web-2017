import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoToolbarComponent } from './ui-demo-toolbar.component';

describe('UiDemoToolbarComponent', () => {
  let component: UiDemoToolbarComponent;
  let fixture: ComponentFixture<UiDemoToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
