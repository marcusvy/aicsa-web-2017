import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoBadgeComponent } from './ui-demo-badge.component';

describe('UiDemoBadgeComponent', () => {
  let component: UiDemoBadgeComponent;
  let fixture: ComponentFixture<UiDemoBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
