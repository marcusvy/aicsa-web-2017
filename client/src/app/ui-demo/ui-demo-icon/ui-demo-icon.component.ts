import { Component, OnInit } from '@angular/core';
import { icons } from './icons';

@Component({
  selector: 'mv-ui-demo-icon',
  templateUrl: './ui-demo-icon.component.html',
  styleUrls: ['./ui-demo-icon.component.scss']
})
export class UiDemoIconComponent implements OnInit {

  icons = icons;
  viewAs: string = 'list'; //list, grid

  constructor() { }

  ngOnInit() {

  }

  setViewAs($view: string) : void {
    this.viewAs = $view;
  }

  getClassViewAs(): Object {
    return {
      'icon-collection--list': (this.viewAs === 'list'),
      'icon-collection--grid': (this.viewAs === 'grid')
    };
  }
}
