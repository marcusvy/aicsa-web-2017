import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoIconComponent } from './ui-demo-icon.component';

describe('UiDemoIconComponent', () => {
  let component: UiDemoIconComponent;
  let fixture: ComponentFixture<UiDemoIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
