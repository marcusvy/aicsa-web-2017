import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoLayoutComponent } from './ui-demo-layout.component';

describe('UiDemoLayoutComponent', () => {
  let component: UiDemoLayoutComponent;
  let fixture: ComponentFixture<UiDemoLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
