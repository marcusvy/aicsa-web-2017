import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoShellComponent } from './ui-demo-shell.component';

describe('UiDemoShellComponent', () => {
  let component: UiDemoShellComponent;
  let fixture: ComponentFixture<UiDemoShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
