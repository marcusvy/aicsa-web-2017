import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoSidebarComponent } from './ui-demo-sidebar.component';

describe('UiDemoSidebarComponent', () => {
  let component: UiDemoSidebarComponent;
  let fixture: ComponentFixture<UiDemoSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
