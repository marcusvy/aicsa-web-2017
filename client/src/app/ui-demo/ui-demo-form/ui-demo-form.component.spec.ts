import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiDemoFormComponent } from './ui-demo-form.component';

describe('UiDemoFormComponent', () => {
  let component: UiDemoFormComponent;
  let fixture: ComponentFixture<UiDemoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiDemoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiDemoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
