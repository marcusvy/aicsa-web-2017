import { MvOrionPage } from './app.po';

describe('mv-orion App', () => {
  let page: MvOrionPage;

  beforeEach(() => {
    page = new MvOrionPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('mv works!');
  });
});
